package com.test.htectest.scheduler

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.test.htectest.database.PostsDatabase
import com.test.htectest.network.proxy.BackendProxy
import com.test.htectest.repository.PostsRepository

class CacheClearingWorker(appContext: Context, params: WorkerParameters) :
    CoroutineWorker(appContext, params) {

    private val backendProxy: BackendProxy by lazy { BackendProxy.create() }
    private val postsDatabase: PostsDatabase by lazy { PostsDatabase.getInstance(appContext) }
    override suspend fun doWork(): Result {
        val postsRepository: PostsRepository = PostsRepository(postsDatabase.postDao(), backendProxy)
        postsRepository.deleteAll()
        val itemCount = postsRepository.getCount()
        return if(itemCount == 0)  Result.success() else Result.failure();
    }
}