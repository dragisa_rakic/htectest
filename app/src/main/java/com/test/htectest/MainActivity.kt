package com.test.htectest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.test.htectest.ui.main.MainFragment
import com.test.htectest.ui.main.PostDetailsFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, MainFragment.newInstance())
                    .commitNow()
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val fragment = supportFragmentManager.findFragmentById(R.id.container)
        if (fragment is PostDetailsFragment) {supportFragmentManager
            supportFragmentManager.popBackStack()
        }
    }
}