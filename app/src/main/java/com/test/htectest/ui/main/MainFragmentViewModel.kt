package com.test.htectest.ui.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.test.htectest.database.PostsDatabase
import com.test.htectest.network.model.post.Post
import com.test.htectest.network.proxy.BackendProxy
import com.test.htectest.repository.PostsRepository
import com.test.htectest.ui.adapter.PostsAdapter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MainFragmentViewModel : ViewModel() {
    private val backendProxy: BackendProxy by lazy { BackendProxy.create() }
    private lateinit var postsRepository: PostsRepository
    private lateinit var adapter: PostsAdapter

    var postList: MutableLiveData<List<Post>>? = MutableLiveData()
    var errorMessage: MutableLiveData<String>? = MutableLiveData()

    fun initDataStack(database: PostsDatabase) {
        postsRepository = PostsRepository(database.postDao(), backendProxy)
    }

    fun initAdapter(postClickresolver: (Post) -> Unit) {
        adapter = PostsAdapter(postClickresolver)
    }

    fun getPosts() {
        viewModelScope.launch(IO) {
            val receivedData = postsRepository.getPosts()
            launch(Dispatchers.Main) {
                with(receivedData) {
                    postList?.value = value
                    if (error != null) {
                        errorMessage?.value = error
                    }
                }
            }
        }
    }

    fun getPostsAdapter(): PostsAdapter {
        return adapter
    }

    fun clearDatabase() {
        viewModelScope.launch(IO) { postsRepository.deleteAll() }
    }
}