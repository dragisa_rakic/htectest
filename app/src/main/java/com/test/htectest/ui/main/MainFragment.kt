package com.test.htectest.ui.main

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import com.test.htectest.R
import com.test.htectest.database.PostsDatabase
import com.test.htectest.network.model.post.Post
import com.test.htectest.scheduler.CacheClearingWorker
import com.test.htectest.ui.adapter.PostsAdapter
import kotlinx.android.synthetic.main.main_fragment.*
import java.util.concurrent.TimeUnit

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: MainFragmentViewModel

    private val errorObserver: Observer<String> = Observer { s:String? ->
        this@MainFragment.showErrorMessage(s)
        refreshPostsLayout.isRefreshing = false
    }

    private val postListObserver: Observer<List<Post>> = Observer { postList: List<Post>? ->
        this@MainFragment.refreshData(postList)
        scheduleCacheClearing()
        refreshPostsLayout.isRefreshing = false
    }

    private val database: PostsDatabase by lazy { PostsDatabase.getInstance(requireContext()) }

    private val pullRefreshListener: SwipeRefreshLayout.OnRefreshListener = SwipeRefreshLayout.OnRefreshListener {
        viewModel.clearDatabase()
        viewModel.getPosts()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(MainFragmentViewModel::class.java)
        viewModel.initDataStack(database)
        viewModel.errorMessage?.observe(viewLifecycleOwner,  errorObserver)
        viewModel.postList?.observe(viewLifecycleOwner, postListObserver)
        viewModel.initAdapter { showDetails(it) }
        rvPostsList.layoutManager = LinearLayoutManager(this.activity)
        rvPostsList.adapter = viewModel.getPostsAdapter()
        refreshPostsLayout.setOnRefreshListener(pullRefreshListener)
    }

    private fun showDetails(post: Post) {
        val targetFragment = PostDetailsFragment.newInstance(post)
        (activity as AppCompatActivity).supportFragmentManager.beginTransaction()
            .replace(R.id.container, targetFragment)
            .addToBackStack(null)
            .commit()
    }

    private fun showErrorMessage(s: String?) {
        Toast.makeText(requireContext(), s, Toast.LENGTH_LONG).show();
    }

    private fun scheduleCacheClearing() {
        val workManager = WorkManager.getInstance(requireContext())
        val delay = 300L
        val workRequest = OneTimeWorkRequestBuilder<CacheClearingWorker>()
            .setInitialDelay(delay, TimeUnit.SECONDS)
            .build()
    }

    private fun refreshData(postList: List<Post>?) {
        if (postList != null) {
            (rvPostsList.adapter as PostsAdapter).loadPosts(postList)
        } else {
            Toast.makeText(requireContext(), "No data to display", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onStart() {
        super.onStart()
        viewModel.getPosts()
    }
}