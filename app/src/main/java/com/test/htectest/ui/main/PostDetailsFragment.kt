package com.test.htectest.ui.main

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import com.test.htectest.R
import com.test.htectest.network.model.post.Post
import com.test.htectest.network.model.user.User
import kotlinx.android.synthetic.main.main_fragment.*
import kotlinx.android.synthetic.main.post_details_fragment.*

class PostDetailsFragment : Fragment() {

    private val POST_DATA = "post_data"
    val post: Post by lazy { arguments?.getSerializable(POST_DATA) as Post }

    companion object {
        fun newInstance(post: Post) = PostDetailsFragment().apply {
            val contentContainer = Bundle()
            contentContainer.putSerializable(POST_DATA, post)
            arguments = contentContainer
        }
    }

    private lateinit var viewModel: PostDetailsViewModel

    private val userObserver: Observer<User> = Observer { user: User? -> this@PostDetailsFragment.showUserData(user)  }

    private fun showUserData(user: User?) {
        userName.text = user?.name
        userEmail.text = user?.email
    }

    private val errorObserver: Observer<String> = Observer { s:String? -> this@PostDetailsFragment.showErrorMessage(s) }

    private fun showErrorMessage(s: String?) {
        Toast.makeText(requireContext(), s, Toast.LENGTH_LONG).show();
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.post_details_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(PostDetailsViewModel::class.java)
    }

    override fun onStart() {
        super.onStart()
        with (post) {
            postTitle.text = title
            postBody.text = body
            viewModel.configureUserDetailsLiveDataSource(userId)
        }
        viewModel.userData?.observe(this, userObserver)
        viewModel.errorMessage?.observe(this,  errorObserver)
        if (post.id != null){
            fabDelete.setOnClickListener {
                viewModel.deletePost(requireContext(), post.id!!)
                activity?.onBackPressed()
            }
        } else {
            fabDelete.isEnabled = false
        }
    }
}