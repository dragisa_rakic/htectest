package com.test.htectest.ui.main

import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.test.htectest.database.PostsDatabase
import com.test.htectest.network.model.FetchResult
import com.test.htectest.network.model.user.User
import com.test.htectest.network.proxy.BackendProxy
import com.test.htectest.repository.PostsRepository
import com.test.htectest.repository.UserDataRepository
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.launch

class PostDetailsViewModel : ViewModel() {

    var userData: MutableLiveData<User>? = MutableLiveData()
    var errorMessage: MutableLiveData<String>? = MutableLiveData()


    fun configureUserDetailsLiveDataSource(userId: Int?) {
        viewModelScope.launch {
            if (userId != null) {
                val queryResult = getUserData(userId)
                launch(Main) {
                    with(queryResult) {
                        userData?.value = value
                        if (error != null)
                            errorMessage?.value = error
                    }
                }
            } else {
                errorMessage?.value = "Missing user ID"
            }
        }
    }

    private suspend fun getUserData(userId: Int): FetchResult<User> {
        return UserDataRepository().fetchUserData(userId)
    }

    fun deletePost(context: Context, postId: Int) {
        val backendProxy: BackendProxy by lazy { BackendProxy.create() }
        val postsDatabase: PostsDatabase by lazy { PostsDatabase.getInstance(context) }
        val postsRepository: PostsRepository =
            PostsRepository(postsDatabase.postDao(), backendProxy)
        viewModelScope.launch(IO) { postsRepository.deletePost(postId) }
    }
}