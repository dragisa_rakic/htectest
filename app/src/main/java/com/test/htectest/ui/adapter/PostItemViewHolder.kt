package com.test.htectest.ui.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.test.htectest.network.model.post.Post
import kotlinx.android.synthetic.main.post_item_layout.view.*

class PostItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bind(item: Post, itemTouchListener: (Post) -> Unit) = with (itemView) {
        postTitle.text = item.title
        idPostFirstRow.text = extractFirstRow(item.body)
        setOnClickListener{itemTouchListener(item)}
    }

    private fun extractFirstRow(item: String?): String? = item?.split("\n")?.get(0)
}
