package com.test.htectest.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.test.htectest.R
import com.test.htectest.network.model.post.Post

class PostsAdapter(private val itemTouchListener: (Post) -> Unit) :
    RecyclerView.Adapter<PostItemViewHolder>() {

    private val items: MutableList<Post> by lazy { mutableListOf() }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostItemViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.post_item_layout, parent, false)
        return PostItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: PostItemViewHolder, position: Int) {
        val item = items[position]
        holder.bind(item, itemTouchListener)
    }

    public fun loadPosts(items: List<Post>) {
        this.items.clear()
        this.items.addAll(items)
        this.notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return items.size
    }
}