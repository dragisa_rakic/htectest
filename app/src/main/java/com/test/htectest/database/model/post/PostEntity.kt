package com.test.htectest.database.model.post

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity(tableName = "posts")
data class PostEntity(
    @PrimaryKey
    var id: Int = 0,
    var userId: Int?,
    var title: String?,
    var body: String?
)