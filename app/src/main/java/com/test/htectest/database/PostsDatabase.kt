package com.test.htectest.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.test.htectest.database.dao.PostDao
import com.test.htectest.database.model.post.PostEntity

@Database(
    entities = [PostEntity::class],
    version = 1,
    exportSchema = false
)

abstract class PostsDatabase : RoomDatabase() {
    abstract fun postDao(): PostDao

    companion object {

        @Volatile
        private var INSTANCE: PostsDatabase? = null

        fun getInstance(context: Context): PostsDatabase =
            INSTANCE ?: synchronized(this) {
                INSTANCE
                    ?: buildDatabase(context).also { INSTANCE = it }
            }

        private fun buildDatabase(context: Context) =
            Room.databaseBuilder(
                context.applicationContext,
                PostsDatabase::class.java, "posts.db"
            )
                .fallbackToDestructiveMigration()
                .build()
    }
}
