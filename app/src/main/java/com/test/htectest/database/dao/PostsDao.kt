package com.test.htectest.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.test.htectest.database.model.post.PostEntity

@Dao
interface PostDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(posts: List<PostEntity>)

    @Query("select * from posts")
    fun load(): List<PostEntity>

    @Query("delete from posts")
    fun deleteAll()

    @Query("delete from posts where id=:discardedId")
    fun delete(discardedId: Int)

    @Query ("select count(*) from posts")
    fun getCount():Int
}