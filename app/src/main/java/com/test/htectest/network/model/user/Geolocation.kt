package com.test.htectest.network.model.user


import com.google.gson.annotations.SerializedName

data class Geolocation(
    @SerializedName("lat")
    val lat: String?,
    @SerializedName("lng")
    val lng: String?
)