package com.test.htectest.network.model

data class FetchResult<T>
    (val value: T? = null,
     val error: String? =null){
}