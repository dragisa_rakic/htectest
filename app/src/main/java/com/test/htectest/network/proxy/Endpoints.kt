package com.test.htectest.network.proxy

object Endpoints {
    const val BASE_URL = "https://jsonplaceholder.typicode.com"
    const val POSTS_ENDPOINT = "/posts"
    const val USER_ENDPOINT = "/users/{userId}"
}