package com.test.htectest.network.model.post

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Post(
    @SerializedName("userId")
    val userId: Int?,
    @SerializedName("id")
    val id: Int?,
    @SerializedName("title")
    val title: String?,
    @SerializedName("body")
    val body: String?
): Serializable