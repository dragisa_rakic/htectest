package com.test.htectest.network.proxy

import com.test.htectest.network.model.post.Post
import com.test.htectest.network.model.user.User
import com.test.htectest.network.proxy.Endpoints.POSTS_ENDPOINT
import com.test.htectest.network.proxy.Endpoints.USER_ENDPOINT
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

interface BackendProxy {

    @GET(POSTS_ENDPOINT)
    suspend fun getPosts(): List<Post>

    @GET(USER_ENDPOINT)
    suspend fun getUserInfo(@Path("userId") userId: Int): User

    companion object RetrofitFactory {
        fun create(): BackendProxy {
            val logging = HttpLoggingInterceptor()
            logging.level = HttpLoggingInterceptor.Level.BODY
            val client = OkHttpClient()
                .newBuilder()
                .addInterceptor(logging)
                .build()

            val retrofit = Retrofit.Builder()
                .baseUrl(Endpoints.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
            return retrofit.create(BackendProxy::class.java)
        }
    }
}