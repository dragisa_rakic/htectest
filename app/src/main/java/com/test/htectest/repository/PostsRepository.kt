package com.test.htectest.repository

import com.test.htectest.database.dao.PostDao
import com.test.htectest.database.model.post.PostEntity
import com.test.htectest.network.model.FetchResult
import com.test.htectest.network.model.post.Post
import com.test.htectest.network.proxy.BackendProxy
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class PostsRepository(
    private val postsDao: PostDao,
    private val backendProxy: BackendProxy) {

    suspend fun getPosts(): FetchResult<List<Post>> {
        try {
            val result = getDatabasePosts()
            return if (result.isEmpty()) {
                try {
                    val posts = getBackendPosts()
                    val entities = convertDtoToEntity(posts)
                    saveToDatabase(entities)
                    FetchResult<List<Post>>(posts)
                } catch (e: Exception) {
                    FetchResult(error = e.message)
                }
            } else {
                FetchResult(convertEntityToDto(result))
            }
        } catch (e: Exception) {
            return FetchResult(error = e.message)
        }
    }

    private fun getDatabasePosts(): List<PostEntity> =
        postsDao.load()

    private fun saveToDatabase(entities: List<PostEntity>) =
        postsDao.save(entities)

    private suspend fun getBackendPosts(): List<Post> =
        withContext(Dispatchers.Default) { backendProxy.getPosts() }


    private fun convertEntityToDto(entities: List<PostEntity>): List<Post> {
        val result = mutableListOf<Post>()
        for (entity: PostEntity in entities) {
            result.add(Post(entity.userId, entity.id, entity.title, entity.body))
        }
        return result
    }

    private fun convertDtoToEntity(posts: List<Post>): List<PostEntity> {
        val result = mutableListOf<PostEntity>()
        for (item: Post in posts) {
            val entity = PostEntity(item.id ?: 0, item.userId, item.title, item.body)
            result.add(entity)
        }
        return result
    }

    fun deleteAll() {
        postsDao.deleteAll()
    }

    fun getCount(): Int {
        return postsDao.getCount()
    }

    fun deletePost(postId: Int) {
        postsDao.delete(postId)
    }
}