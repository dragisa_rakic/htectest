package com.test.htectest.repository

import com.test.htectest.network.model.FetchResult
import com.test.htectest.network.model.user.User
import com.test.htectest.network.proxy.BackendProxy
import java.lang.Exception

class UserDataRepository {
    private val backendProxy by lazy{ BackendProxy.create() }

      suspend fun fetchUserData(userId: Int): FetchResult<User> {
          return try {
              FetchResult(backendProxy.getUserInfo(userId))
          } catch (e: Exception) {
              FetchResult(error = e.message)
          }
    }
}